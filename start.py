#!/usr/bin/env python3

from ctf_site import socketio, app
from ctf_site.config import bind_ip, port

if __name__ == "__main__":
    socketio.run(app, host=bind_ip, port=port)
