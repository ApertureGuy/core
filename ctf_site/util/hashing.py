"""Utilities for handling hash functions."""

from hashlib import sha256


def sha256_digest(flag: str) -> str:
    """Return the digest of a sha256 hash."""
    return sha256(flag.encode()).hexdigest()
