import urllib.parse

import requests

from ..config import public_address, client_id, client_secret


class OAuth:
    SCOPE = "identify"

    # Local routes
    REDIRECT_URI = f"{public_address}/api/login"

    # Remote routes
    DISCORD_API_URL = "https://discordapp.com/api"
    DISCORD_LOGIN_URL = DISCORD_API_URL + "/oauth2/authorize?"
    DISCORD_TOKEN_URL = DISCORD_API_URL + "/oauth2/token"

    @classmethod
    def get_redirect_url(cls):
        # Note: Using `@property` won't work here because we're a classmethod.
        params = dict(
            client_id=client_id,
            redirect_uri=cls.REDIRECT_URI,
            response_type="code",
            scope=cls.SCOPE,
        )

        query = urllib.parse.urlencode(params)
        return cls.DISCORD_LOGIN_URL + query

    @classmethod
    def get_access_token(cls, code):
        payload = dict(
            client_id=client_id,
            client_secret=client_secret,
            grant_type="authorization_code",
            code=code,
            redirect_uri=cls.REDIRECT_URI,
            scope=cls.SCOPE,
        )
        headers = {"Content-Type": "application/x-www-form-urlencoded"}
        r = requests.post(url=cls.DISCORD_TOKEN_URL, data=payload, headers=headers)
        json = r.json()
        return json.get("access_token")

    @classmethod
    def get_user_json(cls, token):
        url = cls.DISCORD_API_URL + "/users/@me"
        headers = {"Authorization": "Bearer {}".format(token)}
        user_json = requests.get(url=url, headers=headers).json()
        return user_json
