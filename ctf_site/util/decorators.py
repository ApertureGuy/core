from functools import wraps

from flask import redirect, request, abort, render_template

from ..leaderboard import leaderboard
from ..models import Challenge, User
from ..config import admins
from .oauth import OAuth


def getBottomBar(token):
    user = User.query.filter_by(auth_token=token).first()
    points = leaderboard.points_for(user)
    solved = int(points / sum(i.points for i in Challenge.query.all()) * 100)
    users = sorted(User.query.all(), key=leaderboard.points_for, reverse=True)
    placement = users.index(user) + 1
    user_data = {
        "name": user.display_name,
        "completion": solved,
        "placement": placement,
        "hints": len(user.hint_used_challenges),
        "score": points,
        "solved": len(user.solved_challenges),
        "firstbloods": leaderboard.get_first_bloods(user),
        "discord_user_id": user.discord_user_id,
    }

    red = int((0xFF0000 * (100 - solved) / 100) // 1)
    red -= red % 0xF0000
    green = int((0xFF00 * (solved) / 100) // 1)
    green -= green % 0xF00
    completion_colour = hex(red + green)
    completion_colour = "#" + completion_colour[2:].zfill(6)
    user_data["completion_colour"] = completion_colour

    return user_data


def user_stats(f):
    """
    Fetches user statistics
    """

    @wraps(f)
    def decorated_function(*args, **kwargs):
        kwargs["user_stats"] = getBottomBar(request.cookies.get("token"))
        return f(*args, **kwargs)

    return decorated_function


def login_required(f):
    """
    Redirects to the login screen if user is not authenticated.
    """

    @wraps(f)
    def decorated_function(*args, **kwargs):
        token = request.cookies.get("token")
        user = User.query.filter_by(auth_token=token).first()
        if user is None:
            return redirect(OAuth.get_redirect_url(), code=302)
        kwargs["user"] = user
        if user.isbanned:
            return render_template("pages/error.html", msg="Your account has been disabled due to a terms of service or code of conduct violation. Please contact an admin for further information.")
        return f(*args, **kwargs)

    return decorated_function


def admin_only(f):
    """
    Restricts access to administrators.
    """

    @wraps(f)
    def decorated_function(*args, **kwargs):
        token = request.cookies.get("token")
        user = User.query.filter_by(auth_token=token).first()
        if user is None or user.discord_user_id not in admins:
            return abort(401)
        return f(*args, **kwargs)

    return decorated_function


def redirect_if_logged_in(f):
    """
    Redirects to the home page if logged in. Used for the login page.
    """

    @wraps(f)
    def decorated_function(*args, **kwargs):
        token = request.cookies.get("token")
        data = User.query.filter_by(auth_token=token).first()
        if data:
            return redirect("/home", code=302)
        return f(*args, **kwargs)

    return decorated_function
