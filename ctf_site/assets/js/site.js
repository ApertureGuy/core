let tracks = [
    "/static/audio/dadada.mp3",
    "/static/audio/main_theme.mp3",
    "/static/audio/underground.mp3",
];

let expectingAudio = false;

function createPopup(title, body) {
    let el = $("<div class='site-msg'>");
    el.append($("<div class='sm-title'>").text(title));
    el.append($("<div class='sm-body'>").text(body));

    el.click(function() {
        $(this).fadeOut(200, function() {$(this).remove()});
    });
    setTimeout(function() {el.fadeOut(1000, function() {$(this).remove()})}, 15000);
    
    $("#site-msg").append(el);
}

function createFeedPopup(title, body) {
    let el = $("<div class='score-popup'>");
    el.append($("<div class='score-popup-title'>").text(title));
    el.append($("<div class='score-popup-body'>").text(body));

    el.click(function() {
        $(this).fadeOut(200, function() {$(this).remove()});
    });
    setTimeout(function() {el.fadeOut(1000, function() {$(this).remove()})}, 15000);

    $("#score_alerts").append(el);
}

function createDonationPopup(username, amount, anonymous) {
    let el = $("<div class='score-popup'><div class='score-popup-title'>Donation</div><div class='score-popup-body'></div><div class='score-popup-body'>Click <a class='open-dp'>here</a> to donate!</div>");
    if (anonymous) {
        el.children(".score-popup-body").eq(0).text(`An anonymous user just donated ${amount}!`);
    } else {
        username = username + " ";
        el.children(".score-popup-body").eq(0).text(`${username} just donated ${amount}!`);
    }

    el.click(function() {
        $(this).fadeOut(200, function() {$(this).remove()});
    });
    setTimeout(function() {el.fadeOut(1000, function() {$(this).remove()})}, 15000);

    $("#score_alerts").append(el);
}

function donationPopup() {
    if (getCookie("didpo") !== "yes") {
        let url = "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=QKK8XCXT6BACS&source=url";
        e = $("<div class='score-popup'><div class='score-popup-title'>Would You Consider Dontaing?</div><div class='score-popup-body'></div></div>")
        e.children(".score-popup-body").html(
            "It looks like you're enjoying this CTF!<br><br>" +
            "This site isn't cheap to run. Would you consider <a class='open-dp'>donating</a> a small amount?<br><br>" +
            "What does this do?<br>" +
            " - Gives you a cool badge<br>" +
            " - Stops the server randomly shutting down<br>" +
            " - Many other benefits! Open the donate popup to see the full list!"
        );

        e.click(function() {
            $(this).fadeOut(200, function() {$(this).remove()});
        });
        $("#score_alerts").append(e);

        // 12 hours
        setCookieSeconds("didpo", "yes", 43200);
    }
}

function createScorePopup(username, challenge_name, isFirst) {
    let e = $("")
    if (!isFirst) {
        e = $("<div class='score-popup'><div class='score-popup-title'>Flag Scored!</div><div class='score-popup-body'></div></div>")
        e.children(".score-popup-body").text(challenge_name + " was captured by " + username + "!");
    } else {
        e = $("<div class='score-popup firstBloodText'><div class='score-popup-title'>Flag Scored!</div><div class='score-popup-body'></div></div>")
        e.children(".score-popup-body").text(challenge_name + " was captured for the first time by " + username + "!");
        if ($("#sfx").attr("src") === "" || $("#sfx").attr("src") === null || $("#sfx").attr("src") === undefined) {
            if (!expectingAudio) {
                $("#sfx").attr("src", "/static/audio/global_firstblood.mp3")[0].play();
                $("#bgm").eq(0).prop('volume', 0.05);
            }
        }
        var meta = document.getElementById("cmeta");
        var ctitle = document.getElementById("ctitle");
        if (meta != undefined && ctitle != undefined && meta != null && ctitle != null) {
            if (meta.textContent === "0 people have solved this challenge." && ctitle.textContent === challenge_name) {
                meta.setAttribute('style', 'white-space: pre;');
                meta.textContent = "1 person has solved this challenge.\r\nFirst solved by " + username;
            }
        }
    }

    e.click(function() {
        $(this).fadeOut(200, function() {$(this).remove()});
    });
    setTimeout(function() {e.fadeOut(1000, function() {$(this).remove()})}, 15000);
    $("#score_alerts").append(e);
}

function createCompletePopup(username) {
    let e = $("")
    e = $("<div class='score-popup firstBloodText'><div class='score-popup-title'>100% Achieved!</div><div class='score-popup-body'></div></div>")
    e.children(".score-popup-body").text(username + " has completed the CTF and achieved 100%!");
    if ($("#sfx").attr("src") === "" || $("#sfx").attr("src") === null || $("#sfx").attr("src") === undefined) {
        if (!expectingAudio) {
            $("#sfx").attr("src", "/static/audio/global_firstblood.mp3")[0].play();
            $("#bgm").eq(0).prop('volume', 0.05);
        }
    }

    e.click(function() {
        $(this).fadeOut(200, function() {$(this).remove()});
    });
    setTimeout(function() {e.fadeOut(1000, function() {$(this).remove()})}, 15000);
    $("#score_alerts").append(e);
}

function randint(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function randomMusic() {
    let track = randint(0, tracks.length - 1);
    let current = $("#bgm").attr("src");
    // Did the track actually change?
    if (current != tracks[track]) {
        if (getCookie("audio") == "yes") {
            $("#bgm").attr("src", tracks[track]);
            $("#bgm")[0].play().then().catch(function() {});
        }
    } else randomMusic();
}

$('#bgm').on("ended", function() {
    console.log("Queuing next track..");
    randomMusic();
});
$('#sfx').on("ended", function() {
    $("#bgm").animate({volume: 1}, 1000);
    $("#sfx").attr("src", "")
});

$("div").on("click", ".site-msg", function() {
    $(this).fadeOut(300, function() {
        $(this).remove()
    });
}).on("click", ".open-dp", function() {
    $("#dp-wrap").fadeIn(300);
}).on("click", "#dp-dark", function() {
    $("#dp-wrap").fadeOut(300);
}).on("click", "#donate-popup", function(e) {
    e.stopPropagation();
}).on("click", "#do-donate", function(e) {
    if ($("#anonymous-donation").is(":checked")) {
        $("#discord_user_id").remove();
    }
    $("#dp-form").submit();
}).on("click", ".switch-button", function(e) {
    if ($(e.target).is("button")) {
        $(this).children("button").removeClass("active");
        $(e.target).addClass("active");

        e.preventDefault();
        e.stopPropagation();
    }
});

$("#support").click(function(){ 
    $("#bgm").eq(0).animate({volume: 0}, 200, function () {
        location.href = "/support";
    });
 });

function sbOption($elem) {
    let children = $elem.children();
    for (let i = 0; i < children.length; i++) {
        if (children.eq(i).hasClass("active"))
            return children.eq(i).data("val");
    }
    return null;
}

function performNav(href, e) {
    if (!href) return;
    if (href.indexOf("http") === 0) return;
    if (href.indexOf("login") !== -1) return;
    if (href.indexOf("/content/") !== -1) return;
    if (href.indexOf("logout") !== -1) return;
    if (href.indexOf("/contributors") !== -1) return;

    if (href) {
        if (e)
            e.preventDefault();
        console.log("Navigated to " + href)

        let bar = $("#progress").width(0).css("opacity", 1);
        let xhr = new window.XMLHttpRequest();
        xhr.addEventListener("progress", function(evt) {
            if (evt.lengthComputable) {
                let percentComplete = evt.loaded / evt.total;
                bar.width(percentComplete + "%");
            } else {
                bar.width("50%");
            }
        } , false);
        $.ajax({
            xhr: function() {
               return xhr;
            },
            type: 'GET',
            url: href,
            data: {},
            success: function(data) {
                let html = $("<html>").html(data);

                bar.width("100%");
                $("title").text(html.find('title').text());

                let bodyHTML = html.find('#load-body').html();
                let scripts = html.find('#scripts').html();

                if (!bodyHTML)
                    return location.href = href;

                stopConfetti();
                $("#load-body").fadeOut(100, function() {
                    $('#load-body').html(bodyHTML).fadeIn(100);

                    ajaxOnDocumentLoad = undefined;
                    $("#scripts").html(scripts);
                    if (ajaxOnDocumentLoad) ajaxOnDocumentLoad();

                    setTimeout(function() {
                        bar.animate({opacity: 0}, 200, function() {
                            bar.width(0);
                        });
                    }, 200);
                });
                history.pushState("", "", xhr.responseURL);
                delete html;
            }, error: function() {
                location.href = href;
            }
        });
    }
}

$(document).on("click", "a", function(e) {
    let href = $(this).attr("href");
    performNav(href, e);
});

$(window).bind("popstate", function() {
    performNav(location.pathname);
});

$("div").on("click", "#hid-df", function(e) {
    e.preventDefault();
    e.stopPropagation();
    let flagdata = $("#hid-fb").val();
    if (!flagdata.length) return;
    expectingAudio = true;
    $.post("/api/attemptFlog", {
            flag: flagdata
        },
        function (data, status) {
            alert(data.m);
            if (data.s) {
                $("#bgm").animate({volume: 0.05}, 300);
                if (!data.f)
                    setTimeout(function() {$("#sfx").attr("src", "/static/audio/victorytheme.mp3")[0].play();}, 300);
                else setTimeout(function() {$("#sfx").attr("src", "/static/audio/firstblood.mp3")[0].play();}, 300);
                startConfetti();
            } else {
                $("#bgm").animate({volume: 0.02}, 300);
                let rand = randint(1, 200)
                if (rand === 1)
                    setTimeout(function() {$("#sfx").attr("src", "/static/audio/failure_2.mp3")[0].play();}, 300);
                else if (rand === 2) {
                    $("#bgm").animate({volume: 0}, 100);
                    setTimeout(function() {$("#sfx").attr("src", "/static/audio/try_harder.mp3")[0].play();}, 400)
                }
                else setTimeout(function() {$("#sfx").attr("src", "/static/audio/failure.ogg")[0].play();}, 300);
            }
            setTimeout(function() {expectingAudio = false;}, 1000);
            if (data.r) {
                performNav(data.r);
            }
        }
    );
});

if (typeof ajaxOnDocumentLoad !== 'undefined') ajaxOnDocumentLoad();
randomMusic();
