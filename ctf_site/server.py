"""Main webctf_page routes."""

import os

import docker
from flask_socketio import emit, SocketIO
from flask import send_from_directory, render_template, redirect, abort, Blueprint, request, make_response

from .util.decorators import redirect_if_logged_in, login_required, user_stats, admin_only
from .docker_routes import sizeof_fmt
from .api_routes import alerts, news
from .util.paths import resolve_path
from .util.oauth import OAuth
from .models import challenge_categories_rosetta, Challenge, User
from .config import admins, countdown, formatting, public_address
from .webhooks import send_webhook
from .contributors import contributors_dict
from .support_routes import support
from .api_routes import alerts

from .leaderboard import leaderboard

ctf_page = Blueprint("ctf_page", __name__, template_folder="templates")
socketio = SocketIO()
client = docker.from_env()


@ctf_page.route("/robots.txt")
def robots():
    response = make_response("User-Agent: *\nCrawl-Delay: 60\nDisallow: /*.json$\nDisallow: /secure/secret_data.txt$\nDisallow: /*.js$", 200)
    response.mimetype = "text/plain"
    return response

@ctf_page.route("/secure/secret_data.txt")
@login_required
def super_secret_data(user):
    return "cdctf{r0boticMemory}"

@ctf_page.route("/")
@redirect_if_logged_in
def root():
    return render_template("formal/splash.html")


@ctf_page.route("/contributors")
def contributors():
    return render_template("formal/contributors.html", contributors=contributors_dict)


@ctf_page.route("/countdown")
@redirect_if_logged_in
def countdown_route():
    return render_template("pages/countdown.html", countdown=countdown)


@ctf_page.route("/welcome")
@login_required
def onboarding(user):
    return render_template("pages/onboarding.html", user=user)


@ctf_page.route("/privacy")
def privacy():
    send_webhook("Pageview", "Someone viewed /privacy owo", "owo who actually reads that shit")
    return render_template("formal/privacy.html", site_address=public_address)


@ctf_page.route("/conduct")
def conduct():
    return render_template("formal/codeofconduct.html")


@ctf_page.route("/about")
def about():
    return render_template("formal/about.html")


@ctf_page.route("/login")
@login_required
def login(user):
    return redirect("/home", code=302)


@ctf_page.route("/join/<referral_code>")
@redirect_if_logged_in
def join_from_referral(referral_code):
    response = make_response(redirect(OAuth.get_redirect_url(), code=302))
    response.set_cookie("referrer", referral_code)
    return response


@ctf_page.route("/home")
@login_required
@user_stats
def home(user, user_stats):
    tempnews = news[::-1]
    user_stats["alertdata"] = tempnews
    return render_template(
        "pages/home.html", **user_stats, is_admin=user.discord_user_id in admins
    )


@ctf_page.route("/user/<name>")
@login_required
@user_stats
def profile_page(user, user_stats, name):
    target = User.query.filter_by(display_name=name).first()
    if target is None:
        target = User.query.filter_by(discord_user_id=name).first()
    if target is None:
        return abort(404)

    return render_template(
        "pages/profile.html", **user_stats, target=target,
        is_admin=target.discord_user_id in admins
    )


@ctf_page.route("/category/<category>/<int:challenge_num>")
@login_required
@user_stats
def challenge(category, challenge_num, user, user_stats):
    challenge = Challenge.query.filter_by(
        category=category, challenge=challenge_num
    ).first()
    if not challenge:
        return render_template("pages/error.html", msg="Challenge does not exist")
    elif challenge.ishidden and challenge not in user.solved_challenges:
        return render_template("pages/error.html", msg="Challenge does not exist")
    else:
        first = None
        if (leaderboard.get_first_solve(challenge) is not None):
            first = User.query.filter_by(discord_user_id=leaderboard.get_first_solve(challenge)).first().display_name

        details = {
            "category": category,
            "challenge": challenge.challenge,
            "title": challenge.title,
            "description": challenge.description.format(**formatting),
            "points": challenge.points,
            "challenge_name": challenge_categories_rosetta[category],
            "solves": len(challenge.users_solved),
            "chal_solved": user in challenge.users_solved,
            "first": first,
            "hint": "",
            "flag_format": challenge.flag_format
        }
        if challenge in user.hint_used_challenges:
            details["hint"] = challenge.hint.format(**formatting)

        return render_template("pages/challenge.html", **details, **user_stats)


@ctf_page.route("/category/<category>")
@login_required
@user_stats
def challenges(category, user, user_stats):
    challenges = Challenge.query.filter_by(category=category).all()
    if not challenges:
        return abort(404)
    return render_template(
        "pages/category.html",
        **user_stats,
        user=user,
        challenges=challenges,
        category=category,
        category_name=challenge_categories_rosetta[category],
        solved_challenges=user.solved_challenges,
    )


@ctf_page.route("/settings")
@login_required
@user_stats
def settings(user, user_stats):
    return render_template("pages/settings.html", user=user, **user_stats)


@ctf_page.route("/hub")
@login_required
@user_stats
def hub(user, user_stats):
    return render_template(
        "pages/hub.html", **user_stats, categories=challenge_categories_rosetta
    )


@ctf_page.route("/leaderboard")
@login_required
@user_stats
def leaderboard_route(user, user_stats):
    return render_template("pages/leaderboard.html", **user_stats, lb_model=leaderboard, leaderboard=leaderboard.get_lbdata())


@ctf_page.route("/admin/")
@admin_only
@login_required
def admin(user):
    return render_template("pages/admin/index.html")


@ctf_page.route("/admin/alerts")
@admin_only
@login_required
def admin_alerts(user):
    return render_template("pages/admin/alerts.html")


@ctf_page.route("/admin/members")
@admin_only
@login_required
def admin_members(user):
    users = sorted(User.query.all(), key=lambda x: x.display_name, reverse=True)

    return render_template("pages/admin/members.html", users=users)


@ctf_page.route("/success")
def success():
    if 'token' in request.cookies:
        anonymous_donation = False
    else:
        anonymous_donation = True
    return render_template("pages/donation-success.html", anonymous=anonymous_donation)

@ctf_page.route("/content/<int:chall_id>")
@login_required
def serverChalContent(chall_id, user):
    files = {
        12: ("misc_challenges/raw_data", "chal.txt"),

        21: ("steganography_challenges/under_you", "photo.jpg"),
        22: ("steganography_challenges/lynesteg", "lynesteg.jpg"),
        23: (
            "steganography_challenges/fricking_weebs",
            "Neon_Genesis_Evangelion-s01e01.mkv",
        ),
        24: (
            "steganography_challenges/the_flag_luke",
            "mspaint_2019-05-06_16-50-33.png",
        ),
        25: ("steganography_challenges/super_beats", "da_da_da_da_dada.wav"),
        26: ("steganography_challenges/queen", "queen.zip"),
        27: ("steganography_challenges", "Aero_Chord_Take_Me_Home.wav"),

        31: ("reversing_challenges/il_part_1", "chal.exe"),
        32: ("reversing_challenges/il_part_2", "chal.exe"),
        33: ("reversing_challenges/easy_dotnet", "challenge.zip"),
        34: ("reversing_challenges", "game.pyc"),
        35: ("reversing_challenges/decaf", "challenge.txt"),

        46: ("web_challenges", "capture.pcapng"),

        62: ("crypto_challenges", "postit.zip"),
        641: ("crypto_challenges", "client.py"),
        642: ("crypto_challenges", "capture.pcapng"),
        65: ("crypto_challenges", "email_intercept.txt"),
        661: ("crypto_challenges", "intercept.cap"),
        662: ("crypto_challenges", "intercept_gpu.hccapx"),
        67: ("crypto_challenges", "ternary.txt"),
        68: ("misc_challenges/transmission_recv", "TRANSMISSIONRECIEVED.wav"),
        69: ("crypto_challenges", "scrabble_scramble.txt"),
        70: ("misc_challenges/spooky_mixtapes", "SPOOKYMIXTAPES.wav"),
        71: ("misc_challenges/genesis", "genesis.txt"),
        72: ("misc_challenges/sec_obsc", "scrapedpartial.html")
    }
    if chall_id in files:
        directory = os.path.abspath(
            resolve_path(
                "..", "challenges", files[chall_id][0]
            )
        )
        return send_from_directory(directory, files[chall_id][1], as_attachment=True)
    return abort(404)


@ctf_page.route("/static/<path:path>")
def send_static(path):
    return send_from_directory("static", path)


@ctf_page.route("/favicon.ico")
def send_favicon():
    return send_from_directory("static/img", "favicon.ico")


@ctf_page.after_request
def denyIframes(response):
    response.headers["X-Frame-Options"] = "DENY"
    return response


@socketio.on("connect", namespace="/")
def on_client_connect():
    for i in alerts:
        emit("alert", i)


@socketio.on("request_container_usage", namespace="/docker")
def handle_request_container_usage(json):
    token = json.get("token", "")
    user = User.query.filter_by(auth_token=token).first()
    if user is None or user.discord_user_id not in admins:
        return abort(401)
    try:
        container = client.containers.get(json.get("container", ""))
    except docker.errors.NotFound:
        return abort(404)

    stats = container.stats(stream=False)
    memory = stats.get("memory_stats", {}).get("usage", 0)
    cpu = stats.get("cpu_stats", {})
    pcpu = stats.get("precpu_stats", {})

    cpu_delta = cpu.get("cpu_usage", {}).get("total_usage", 0) - pcpu.get(
        "cpu_usage", {}
    ).get("total_usage", 0)
    system_delta = cpu.get("system_cpu_usage", 0) - pcpu.get("system_cpu_usage", 0)

    cpu_usage = round(cpu_delta / system_delta * 100, 2)
    memory = sizeof_fmt(memory)

    tag = container.image.tags[0].split(":")[0]
    path = resolve_path("..", "challenges", "Logs", tag.replace("/", "-") + ".log")
    if os.path.exists(path):
        with open(path) as log_file:
            logs = log_file.read()
    else:
        logs = "<No content>"

    logs = logs.split("\n")
    try:
        lines = int(json.get("recieved_log_lines", len(logs)))
        extra_log_lines = logs[max(0, lines - 2):]
    except ValueError:
        extra_log_lines = []

    extra_log_lines = "\n".join(extra_log_lines)
    emit(
        "response_container_usage",
        {"cpu_usage": cpu_usage, "memory_usage": memory, "extra_log": extra_log_lines},
        namespace="/docker",
    )
