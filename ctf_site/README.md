# Website itself

List of current routes (as of 16.05.19)

 - `api.remove_alert_endpoint` provides `/api/admin/alerts/remove`
 - `api.add_alert` provides `/api/admin/alerts/add`
 - `api.unban_user` provides `/api/admin/unban`
 - `api.admin_remc` provides `/api/admin/remc`
 - `api.ban_user` provides `/api/admin/ban`
 - `ctf_page.robots_flag` provides `/secure/secret_data.txt`
 - `challenge_editor.docker_index` provides `/admin/challenge_editor/`
 - `ctf_page.admin_members` provides `/admin/members`
 - `ctf_page.admin_alerts` provides `/admin/alerts`
 - `api.attemptFlag` provides `/api/attemptFlag`
 - `api.changeName` provides `/api/changeName`
 - `api.logout` provides `/api/logout`
 - `api.discordlogin` provides `/api/login`
 - `api.getHint` provides `/api/hint`
 - `ctf_page.leaderboard` provides `/leaderboard`
 - `ctf_page.send_favicon` provides `/favicon.ico`
 - `ctf_page.serve_robots` provides `/robots.txt`
 - `ctf_page.countdown_route` provides `/countdown`
 - `ctf_page.settings` provides `/settings`
 - `ctf_page.privacy` provides `/privacy`
 - `ctf_page.conduct` provides `/conduct`
 - `docker_routes.docker_index` provides `/docker/`
 - `ctf_page.about` provides `/about`
 - `ctf_page.login` provides `/login`
 - `ctf_page.admin` provides `/admin/`
 - `ctf_page.home` provides `/home`
 - `ctf_page.hub` provides `/hub`
 - `ctf_page.root` provides `/`
 - `challenge_editor.save_configuration` provides `/admin/challenge_editor/save/<category>/<int:challenge_num>`
 - `docker_routes.docker_logs` provides `/docker/logs/<container>`
 - `docker_routes.docker_stop_container` provides `/docker/stop/<container>`
 - `challenge_editor.edit_challenge` provides `/admin/challenge_editor/<category>/<challenge_num>`
 - `challenge_editor.view_challenges` provides `/admin/challenge_editor/<category>`
 - `api.get_avatar` provides `/api/avatar/<avatar>`
 - `ctf_page.challenge` provides `/category/<category>/<int:challenge_num>`
 - `ctf_page.challenges` provides `/category/<category>`
 - `ctf_page.serverChalContent` provides `/content/<int:chall_id>`
 - `ctf_page.send_static` provides `/static/<path:path>`
 - `ctf_page.profile_page` provides `/user/<name>`
 - `static` provides `/<path:filename>`

## Updating this file:

Attach a debugger to a paused instance of the server and run the following:
```py
>>> from ctf_site import app
>>> for i in app.url_map.iter_rules():
...     print(f' - `{i.endpoint}` provides `{i.rule}`')
...
<output above>
```